//import React, { Component } from 'react';
//import {
//  AppRegistry
//} from 'react-native';
//
//import { Provider, connect } from "react-redux";
//import { StackNavigator, addNavigationHelpers } from 'react-navigation';
//
//import Map from './src/map';
//import Routes from "./src/config/routes";
//
//import getStore from "./src/store";
//
//const AppNavigator = StackNavigator(Routes, {
//    navigationOptions: {
//        title: ({ state }) => {
//            if (state.params) {
//                return `${state.params.title}`;
//            }
//        }
//    }
//});
//
//const navReducer = (state, action) => {
//    const newState = AppNavigator.router.getStateForAction(action, state);
//    return newState || state;
//};
//
//connect(state => ({
//    nav: state.nav
//}));
//
//class AppWithNavigationState extends Component {
//    render() {
//        return (
//            <AppNavigator
//                navigation={addNavigationHelpers({
//                    dispatch: this.props.dispatch,
//                    state: this.props.nav
//                })}
//            />
//        );
//    }
//}
//
//const store = getStore(navReducer);

//
//export default class findMyCarAnywhere extends Component {
//    static navigationOptions = {
//        title: 'Find you car anywhere',
//    };
//    render() {
//        const { navigation } = this.props;
//        return (
//            <Provider store={store}>
//                <Map navigation={ navigation } />
//            </Provider>
//    );
//  }
//}

//export default function findMyCarAnywhere() {
//    return (
//        <Provider store={store}>
//            <AppWithNavigationState />
//        </Provider>
//    );
//}
//
//AppRegistry.registerComponent('findMyCarAnywhere', () => findMyCarAnywhere);
//



import { AppRegistry } from "react-native";
import App from "./src";

AppRegistry.registerComponent("findMyCarAnywhere", () => App);
