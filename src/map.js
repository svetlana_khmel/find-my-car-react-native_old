import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  PixelRatio
} from 'react-native';

import MapView, { Marker, Circle, Polyline } from 'react-native-maps';
import pick from 'lodash/pick';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as dataActions from "./actions/data";

import Button from './components/button'

var { width, height } = Dimensions.get('window');

//To calculate latiudeDelta/longDelta
const earthRadiusInKM = 6371;
// you can customize these two values based on your needs
const radiusInKM = 0.1;
const aspectRatio = 0.1;

class Map extends Component {
    constructor () {
        super();

        this.state = {
            region: {
               latitude: 0,
               longitude: 0,
               latitudeDelta: 0,
               longitudeDelta: 0,
             },
             destination: {},
             markers: [],
             carMarketSet: false,
             routeCoordinates: [],
             distanceTravelled: 0,
             prevLatLng: {},
             circles: [{
                  center: {
                    latitude: 0,
                    longitude: 0
                  },
                  MapCircle: {
                      latitude: 0,
                      longitude: 0
                  },
                  radius: 50,
                  fillColor: '#2196f3',
                  strokeColor: '#f44336'
             }],
             polylines: [

             ],
             currentPosition: {
                latitude: 0,
                longitude: 0,
             },
             polylineCoordiantes: []
        };

        this.centerMe = this.centerMe.bind(this);
        this.centerCar = this.centerCar.bind(this);
        this.showRoute = this.showRoute.bind(this);
        this.getCurrentPosition = this.getCurrentPosition.bind(this);
        this.getWatchPosition = this.getWatchPosition.bind(this);
        this.removeMarker = this.removeMarker.bind(this);
        this.decode = this.decode.bind(this);
        this.getCoordinates = this.getCoordinates.bind(this);

        this.addMarker = this.addMarker.bind(this);

        this.deg2rad = this.deg2rad.bind(this);
        this.rad2deg = this.rad2deg.bind(this);
        this.showRegion = this.showRegion.bind(this);

        this.getDirections = this.getDirections.bind(this);
        this.setDataToPoliline = this.setDataToPoliline.bind(this);
        this.getAlternativeCoords = this.getAlternativeCoords.bind(this);
    }

    static navigationOptions = {
          title: 'Map',
    };

    componentDidMount () {
        console.log("Component did mount....");
        this.getCurrentPosition();
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    getCoordinates (origin, destination) {
       this.props.fetchCoordinates(origin, destination).then(data => {

           console.log("....................getCoordinates  ", data);

           if (data.routes.length) {
               debugger;
               this.setState({
                   coords: this.decode(data.routes[0].overview_polyline.points) // definition below
               });
           }
        });
    }

    getCurrentPosition (setMarker) {
        console.log('____ getCurrentPosition');

        navigator.geolocation.getCurrentPosition((position) => {

            console.log("Position... ", position);
            let coords = {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
            }

            this.setState({
               region: {
                    longitude: coords.longitude,
                    latitude: coords.latitude,
                    latitudeDelta: this.state.region.latitudeDelta,
                    longitudeDelta: this.state.region.longitudeDelta
                },
                destination: {
                    longitude: coords.longitude,
                    latitude: coords.latitude
                }
             });

             if (setMarker === true) {
                 this.addMarker({coords});
             }

             this.getWatchPosition();
        }, (error) => {
            console.log(JSON.stringify(error));

            this.getAlternativeCoords(setMarker);
        }, {
            maximumAge:60000,
            timeout:1000,
            enableHighAccuracy: true
        });
    }

    getAlternativeCoords (setMarker) {
        console.log("MAP PROPS", this.props);

        this.props.fetchAlternativeCoords().then(response => {
               let loc = response.loc.split(',');
               let coords = {
                   latitude: parseFloat(loc[0]),
                   longitude: parseFloat(loc[1])
               };

               this.showRegion(coords);

               if (setMarker === true) {
                   this.addMarker(coords);
               }
            });
    }

    getWatchPosition () {

        console.log('___ getWatchPosition');

        navigator.geolocation.watchPosition((position) => {

            const { routeCoordinates } = this.state
            const positionLatLngs = pick(position.coords, ['latitude', 'longitude']);

            this.setState({
                routeCoordinates: routeCoordinates.concat(positionLatLngs),
            });

            this.showRegion({longitude: position.coords.longitude, latitude: position.coords.latitude});
            //this.setDataToPoliline() // Draw directions
        }, (error) => {
            this.getAlternativeCoords();
            console.log(JSON.stringify(error));
        }, {
            enableHighAccuracy: true
        });
    }

    showRegion(locationCoords) {
        var radiusInRad = radiusInKM / earthRadiusInKM;
        var longitudeDelta = this.rad2deg(radiusInRad / Math.cos(this.deg2rad(locationCoords.latitude)));
        var latitudeDelta = aspectRatio * this.rad2deg(radiusInRad);

        this.setState({
          region: {
                longitude: locationCoords.longitude,
                latitude: locationCoords.latitude,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
            },
            circles: [{
                latitude: locationCoords.longitude,
                longitude: locationCoords.latitude,
                radius: 50,
                fillColor: '#000',
                strokeColor: '#333',
                center: {
                    latitude: locationCoords.longitude,
                    longitude: locationCoords.latitude
                }
            }]
          });
    }

    deg2rad (angle) {
        return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
    }

    rad2deg (angle) {
        return angle * 57.29577951308232 // angle / Math.PI * 180
    }

    getDirections() {
       //Alternative to google: https://api.mapbox.com/v4/directions/mapbox.driving/-118.2980330,34.1036520;-118.258443,34.145785.json?geojson=true&access_token=YOUR_API_TOKEN
        console.log('#####  ____', this.state.routeCoordinates)
        //let destination = this.state.region.latitude +','+ this.state.region.longitude;
        let destination = this.state.destination.latitude + ',' + this.state.destination.longitude;
        let origin = this.state.region.latitude +','+ this.state.region.longitude;
        let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}`;
        //let url = `https://api.mapbox.com/v4/directions/mapbox.walking/${origin};${destination}.json?geojson=true&access_token=${MapBoxApiKey}`
        return new Promise((resolve, reject) => {
          fetch(url)
          .then((response) => {
            console.log('Get data for direction...response', response)
            return response.json();
          }).then((json) => {
            resolve(json);

            console.log('Get data for direction...', json);

          }).catch((err) => {
            reject(err);
          });
        });
    }

    setDataToPoliline () {
        this.getDirections().then((result) => {
               let polylineCoordiantes = result.routes[0].geometry.coordinates.map(c => {
                  return {
                     latitude: c[1],
                     longitude: c[0],
                  }
              });

              this.setState({
                polylineCoordiantes: polylineCoordiantes
              })
         });
    }

//    renderRoute (coordinates, i) {
//      return (
//        <MapView.Polyline
//          key={i}
//          coordinates={coordinates}
//          strokeColor={'red'}
//          strokeWidth={4}
//        />
//      );
//    }

    decode (t,e){for(var n,o,u=0,l=0,r=0,d= [],h=0,i=0,a=null,c=Math.pow(10,e||5);u<t.length;){a=null,h=0,i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);n=1&i?~(i>>1):i>>1,h=i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);o=1&i?~(i>>1):i>>1,l+=n,r+=o,d.push([l/c,r/c])}return d=d.map(function(t){return{latitude:t[0],longitude:t[1]}})}
    //
    centerMe () {
        let map = [
            {
                latitude: this.state.region.latitude,
                longitude: this.state.region.longitude
            }
        ]

        this.map.fitToCoordinates(map, {
              animated: true,
        });
    }

    centerCar () {
        let map = [
            {
                latitude: this.state.region.latitude,
                longitude: this.state.region.longitude
            },

            {
                latitude: this.state.destination.coordinate.latitude,
                longitude: this.state.destination.coordinate.longitude
            }
        ]

        this.map.fitToCoordinates(map, {
              animated: true,
        });
    }

    showRoute () {

    }

    addMarker (coords) {
        let origin = this.state.region.latitude +','+ this.state.region.longitude;
       //let destination = e.nativeEvent.coordinate.latitude +','+ e.nativeEvent.coordinate.longitude;
let destination = this.state.destination.latitude + 5 +','+ this.state.destination.longitude + 5;

        if (this.state.markers.length != 1) {
            this.setState({
                    markers: [
                       // ...this.state.markers, //Because we need only one marker
                        {
                           // coordinate: e.nativeEvent.coordinate,
                           coordinate: this.state.region
                           // cost: `$${getRandomInt(50,300)}`
                        }
                    ],
                    carMarketSet: true,
                    destination: {
                        coordinate: this.state.region
                    }
                });

                this.getCoordinates(origin, destination);
                //this.getCoordinates(url);
        }
    }

    removeMarker () {
        //this.state.markers.length = 0;

        this.setState({
            markers: [],
            carMarketSet: false
        })
        console.log('....this.state.markers', );
    }

    render() {
        //const { region } = this.props;
        console.log(this.state.region);

        return (
          <View style={styles.container}>
            <View style={styles.buttonContainer}>
                  <Button val={'Where I am'} onPress={() => this.centerMe()} />

                  {this.state.carMarketSet === true &&
                    <Button val={'Where my car is?'} onPress={() => this.centerCar()} />
                  }

                  {this.state.carMarketSet === false &&
                    <Button val={'Add marker'} onPress={() => this.getCurrentPosition(true)} />
                  }

                  {this.state.carMarketSet === true &&
                      <Button val={'Remove marker'} onPress={() => this.removeMarker()} />
                  }
            </View>

            <MapView
              ref={ref => { this.map = ref; }}
              style={styles.map}
              region={this.state.region}
              showsUserLocation={true}
              followUserLocation={true} //map will focus on the user's location
              showsMyLocationButton={true}
              showsCompass={true}
              showsScale={true}
              showsTraffic={true}
              zoomEnabled={true}
              loadingEnabled={true}
              loadingIndicatorColor={'#606060'}
              overlays={[{
                  coordinates: this.state.routeCoordinates,
                  strokeColor: '#19B5FE',
                  lineWidth: 10,
                }]}
            >
                {this.state.markers.map((marker, index)=> {
                    return (
                        <Marker {...marker} style={styles.marker} key={index} draggable image={require('./images/Car.png')} onDragEnd={(e) => this.setState({ x: e.nativeEvent.coordinate })}>
                            <View>
                                <Text></Text>
                            </View>
                        </Marker>
                    )
                })}

                <Polyline coordinates={this.state.polylineCoordiantes} />

                {this.state.circles.map((circle, index) => {
                    <Circle {...circle}/>
                })}

            </MapView>
          </View>
        );
      }
}


export default connect(
    state => ({
        data: state.coordinates
    }),
    dispatch => bindActionCreators(dataActions, dispatch)
)(Map);

//Map.navigationOptions = {
//  title: 'Add marker to mark your parking place',
//};

const styles = StyleSheet.create({
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  map: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 60,
    bottom: 0
  },
  button: {
    borderWidth: 1/PixelRatio.get(),
    borderColor: "#995aba",
    borderRadius: 10,
    backgroundColor: '#995aba',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    height: 40
  },
  buttonText: {
    fontSize: 14,
    color: "#fff",
    margin: 10
  },
  marker: {
    backgroundColor: "#550bbc",
    padding: 5,
    borderRadius: 5
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60
  }
});


