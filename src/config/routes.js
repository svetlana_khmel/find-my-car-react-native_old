import Help from '../help';
import Map from '../map';

const Routes = {
     Home: { screen: Map },
     Help: { screen: Help, title: 'Go to Help' }
};

export default Routes;
