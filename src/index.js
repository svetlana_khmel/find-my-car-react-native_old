import React, { Component } from "react";
import { Text } from "react-native";
import { Provider, connect } from "react-redux";
import { DrawerNavigator, addNavigationHelpers } from "react-navigation";

import Routes from "./config/routes";

import getStore from "./store";

const AppNavigator = DrawerNavigator(Routes, {
    navigationOptions: {
        title: ({ state }) => {
            if (state.params) {
                return `${state.params.title}`;
            }
        }
    }
});

const navReducer = (state, action) => {
    const newState = AppNavigator.router.getStateForAction(action, state);
    return newState || state;
};

@connect(state => ({
    nav: state.nav
}))
class AppWithNavigationState extends Component {
    render() {
        return (
            <AppNavigator
                navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav
                })}
            />
        );
    }
}

// wraps dispatch to create nicer functions to call within our component
//const mapDispatchToProps = (dispatch) => ({
//  dispatch: dispatch,
//  startup: () => dispatch(StartupActions.startup())
//})
//
//const mapStateToProps = (state) => ({
//  nav: state.nav
//})

const store = getStore(navReducer);

export default function NCAP() {
    return (
        <Provider store={store}>
            <AppWithNavigationState />
        </Provider>
    );
}
