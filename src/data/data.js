const APIKEY = 'AIzaSyA4kWmwg4AQSzYeC5B1wlbqagOfFRf9urE';
const serviceAccountID = '1014c94c35475b34e09ba835d9fe6fdc66f038fe';
const MapBoxApiKey = 'pk.eyJ1Ijoic3ZpdGxhbmFraCIsImEiOiJjajRqcXYxeGgwYzR5MndxczE4dXo0MGpmIn0.YkSeGKHIrAyRl1awdhgTkg';
const altermativeCoordsUrl = 'https://ipinfo.io/geo';

let directionsURL = `https://maps.googleapis.com/maps/api/directions/json`;
const mode = 'walking'; // 'driving';
//let directionsURL = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}`;

const BASE_URL    = 'https://www.nhtsa.gov/webapi/api/SafetyRatings/',
      BASE_PARAMS = 'format=json';

const data  = {
    getCoordinates (origin, destination) {
        debugger;
        return fetch(`${directionsURL}?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}`);
        //return fetch(`${BASE_URL}?${BASE_PARAMS}`);
    },
    getAlternativeCoordinates () {
        return fetch(`${altermativeCoordsUrl}`);
    }
}

export default data;