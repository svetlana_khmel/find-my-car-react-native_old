import { combineReducers } from "redux";
import data from "./data";

export default function getRootReducer(navReducer) {
    return combineReducers({
        nav: navReducer,
        data: data
    });
}