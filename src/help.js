import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  PixelRatio
} from 'react-native';

class Help extends Component {
    constructor () {
        super();
    }

    render() {

        return(
             <View style={styles.container}>
                    <Text style={styles.header}>How to use this application:</Text>
                     <Text style={styles.regularText}>* Tap "Add marker" button and marker will be added right on the place you are staying at.</Text>
                     <Text style={styles.regularText}>* Tap "Where I am" to center map on you position.</Text>
                     <Text style={styles.regularText}>* Tap "Where my car is" to center map on route to you car.</Text>
             </View>
        )
    }
}

Help.navigationOptions = {
  title: 'How to use',
};

export default Help;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#F5FCFF'
    },
    regularText:{
        fontSize: 16,
        color: "#000",
        margin: 20,
        marginTop: 5,
        marginBottom: 5
    },
    header:{
        fontSize: 24,
        color: "#FF9800",
        margin: 20,
        marginTop: 5,
        marginBottom: 5
    },

});