import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  PixelRatio,
  Button
} from 'react-native';

import { StackNavigator } from 'react-navigation';

//import Button from './components/navButton';

import Map from './map';
import Help from './help';

class Navigation extends Component {
    constructor () {
        super();
    }

    static navigationOptions = {
        title: 'Chat with Lucy'
    };

    render() {
        const { navigate } = this.props.navigation;

        return (
           <View style={styles.container}>

            <Button
              onPress={() => navigate('Map')}
              title="Map"
              style={styles.regularButton}
            />
            <Button
              onPress={() => navigate('Help')}
              title="Help"
              style={styles.regularButton}
            />
           </View>
        );
    }
}

export default Navigation;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    regularButton: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#26a69a",
        borderRadius: 10,
        backgroundColor: '#26a69a',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    textRegularButton: {
       fontSize: 24,
       color: "#fff",
       margin: 10
    }
});